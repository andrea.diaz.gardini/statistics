# CheatSheet

que hace | comando
--|--
__generar número aleatorios__  |  `sample(min:max,cantidad)` <br/> `sample(min:max, cantidad, replace=TRUE) <- para que no se repitan los numeros` <br/>
__generar números aleatorios racionale__  | `runif(num)` <br/>`num * runif <- para que sea de 0 a num` <br/> `num * runif(n) + m <- seria igual a sample(m: num + m) en números racionelas`<br/>`runif(cantidad, min, max)`
__el número superior a *x* __<br/>imprime un número <u>entero</u> superior al que le das | `celing(x)`


## vector to column

```
> datos = c(12, 12, 13, 14, 14, 15, 15, 16, 18, 19)
> datos []
  [1] 12 12 13 14 14 15 15 16 18 19
> datos <- data.frame(datos)
> datos
   datos
1     12ÇÇ
2     12
3     13
4     14
5     14
6     15
7     15
8     16
9     18
10    19
```

## get all the operations

```
> summary (datos)
     datos
 Min.   :12.00
 1st Qu.:13.25
 Median :14.50
 Mean   :14.80
 3rd Qu.:15.75
 Max.   :19.00
```

## **get the number of times a thing repeated**
```
> encuesta = c("rojo", "amarillo", "rojo", "verde", "rojo", "violeta", "verde", "verde")
> table(encuesta)
encuesta
amarillo     rojo    verde  violeta
       1        3        3        1
```

## **make bar graphic**

```
> barplot(table(encuesta))
// with colors //
> barplot(table(encuesta), col=rainbow(length(encuesta)))
//defining by row //
> barplot(table(encuesta), col=c("yellow", "blue", "red", "green")
// with titles//
> barplot(table(encuesta), main = "Grafica", xlab = "colores", ylab = "frecuencia")
```

## **make plot graphic** *(cajas y bigotes)*

```
> plot(data$columny, data$columnx)
// with colors //
> plot(data$columny, data$columnx, col = rainbow(lenght(data)))
```

## **make a histogram graphic**

```
> hist (data$column)
> hist (data$columnx, data$columny)
```

## **save history**

```
> savehistory("Comandos")
```

## **open csv file**
it has to be in the same folder

```
> banco <- read.csv("name_of_file.csv", sep = ";")
```

## **crear una funcion**
7 lanzamientos de un dado de 6 caras
```
> lanza_dado <- function (n, k) {
    return (sample(1:k, n T))
  }
> lanza_dado(7, 6)
```

Implementar una función en R que permita visualizar llos resultados de todos los lanzamientos que efectue (de un dado de 6 caras) hasta que se optenga el número 6. La función también debe mostrar el número de lanzamientos hasta obtener el número 6.

```
> rand <- function() {
+ a = 0
+ b = sample(1:6, 1, T)
+ while (b != 6) {
+ a = a + 1
+ b = sample(1:6, 1, T) }
+ return (a) }
> rand()
```

## Hallar probabilidad de una cantidad cuando ya te dan una probabilidad
**dbinom(3, 7,0.2)** representa la probabilidad de obtener 3 exitos al efectuar 7 pruebas de Bernoulli, donde la probabilidad de exito de cada prueba es 0.2.
```
> dbinom (cantidad de hallar, cantidad total, probabilidad);

```

representar la probabilidad de obtener hasta 3 exitos al efectuar 7 pruebas de Bernoulli, donde la probabilidad de exito de cada prueba es 0.2.
```
> dbinom(3, 7, 0.2) + dbinom(2, 7, 0.2) + dbinom(1, 7, 0.2) + dbinom(0, 7, 0.2)
 // or
> valores = sum(dbinom(3:0, 7, 0.2))
> valores
[1] 0.966656
```

cual es la probabilidad de obtener mas de 5 exitos al efectuar 7 pruebas de Bernoulli, donde la probabilidad de exito de cada prueba Bernoulli es igual a 0.2
```
> valores = sum(dbinom(6:7, 7, 0.2))
```

## pbinom 
```
pbinom( x, size = n, prob = p, lower.tail = T)
pbinom (x,n,p)
```

donde: 
	-	n representa el numero de Pruebas de Bernoulli.
	-	p es la probabilidad de exito para cada una de las pruebas
	-	T es el valor por defecto del paramentro booleano lower.tail,
La funcion devolvera la probabilidad de que la cantidad de exitos sea menor o igual al valor espespecificado por x.

```
> pbinom (8, 12, 0.3, T)
[1] 0.001691655
> sum(dbinom(9:12, 12, 0.3))
[1] 0.001691655
```

### ejericios
1.	Distribucion binomial:
	Supongamos que el 5% de los articulos producidos en una fabrica sin defectuosos. Hallar la probabilidad p de que hayan 92 articulos no defectuosos en una muestra de 100 articulos.
	n = 100
	exito = obtener articulo no defectuoso
	p = 0.95
	x = 92
	```
	> dbinom(92, 100, 0.95)
	[1] 0.06487089
	```

4. Supongamos que el 5% de los articulos producidos en una fabrica son defectuosos. Hallar la probabilidad P de que menos de 15 o mas de 83 articulos no dfectuosos en una muestra de 100 articulos.
	n = 100
	exito = obtener articulo no defectuoso
	p = 0.95
	x < 15 || x > 83

	```
	> sum(dbinom(0:14, 100, 0.95)) + sum(dbinom(84:100, 100, 0.95))
	[1] 0.9999978
	> pbinom(14, 100, 0.95) + pbinom(82, 100, 0.95, F)
	// x <= 14								x > 83
	[1] 0.9999978
	```


# Distribucion Binomial Negativa
Teniendo una sucesion de n pruebas Bernoulli: EEEEEEFEEEEF...
Considerando r como un numero entero positivo
Se define la Variable Aleatoria Discreta X como un numero K de experimentos realizados antes de obtener el r-esimo exito,

Ejemplo:
	si r = 3
	X(EEE) = 3
	X(EFEE) = 4
	X(FEEFE) = 5

	notamos que K = r, r+1, r+3, ...

## dnbinom
```
dnbinom (k-r, r, p)
```

Donde:
-	k representa el numero de pruebas de bernoulli hasta obtener el r-esimo exito.
-	p es la probabilidad de exit para cada una de las pruebas bernoullo
`dnbinom(k-r, r, p` representa la probabilidad de tener que sea necesario realizar una cantidad menor o igual

### ejercicios
1.	En una fabrica que produce sensores sensores medidores de distancia, estos tienen una probabilidad de 0.9 de pasar el control de calidad.
	Calcule la probabilidad de que sean necesarios de revisar 5 sensores para que 3 pases el control de calidad.
	p = 0.9
	exito = el sensor pasa el control de calidad
	r = 3
	k = 5
	n = 0.04374

	**formula**
	(^(5 - 1), (3 - 1)0.9^3 * (1 - 0.9)^(5 - 3) = 0.04374

2.	En una fabrica que produce sensores medidores de distancia, estos tienen una probabilidad de 0.95 de pasar el control de calidad.
	Calcule la probabilidad de que se presenten 4 sensore fallidos antes de que se presente el sexto sensor que pasa el control de calidad.
	p = 0.95
	exito = el sensor pasa el control de calidad
	r = 6
	k = 10 (# de experimentos hasta alcanzar el r-esimo exito)
	(n - r) = # de fracasos hasta/antes_de alcanzar el r-esimo exito

	**formula**
	(10 - 1)! / (6 - 1)! * 0.95^6 * (1 - 0.95)^(10 - 6) = 


## dgeom(k, p)
Donde:
-	k representa el numero de pruebas de bernoulli antes de que se produce el exito por primera vez.
-	p es la probabilidad de exito de cada prueba de bernoulli.
-	`dgeom(k, p) representa la probabilidad de sean necesario realizar exactamente k experimentos antes de obtener el primer exito.

### Ejemplo:
dgeom(10, 0.2) representa la probilidad de que sea necesario realizar exactamente 10 pruebas antes de obtener el primer exito (es decir, en la prueba 11 se logra el exito). Donde cada una de las pruebas de Bernoulli tiene una probabilidad de exito de 0.2
	```
	> dgeom(99, 0.04)
	[1] 0.00070293
	>(0.96^99) * (0.04)
	[1] 0.00070293
	```

## pgeom
```
pgeom(k, p)
```

Donde:
-	k representa el numero maximo de pruebas de Bernoulli antes de que se produce el exito por primera vez.
-	p es la probabilidad de exito de cada prueba de Bernoulli.
-	pgeom(k, p) representa la probabilidad de sean necesaio realizar un numero menor o igual de k pruebas antes de obtener el primer exito.

### Ejemplo:
	`pgeom(10, 0.2)` representa la probabilidad de que sea necesario realizar un numero menor o igual a 10 pruebas antes de obtener el primer exito. Donde cada una de las pruebas de Bernoulli tiene una probabilidad de exito de 0.2.

	```
	> pgeom(100, 0.04)
	[1] 0.9838045
	> sum(dgeom(0:100, 0.04)
	```

# Densidad
  f(x) = landa*e^(-landa*x))

# Distribucion
Para todos los x que me den antes 
  F(x) = 1 - e^(-landa*x)

# dexp(x, L)
Donde:
-   "L" es el parámetro de las Función Exponencial
-   "x" es el valor para el cual se desea evaluar la función de Densidad
-   `dexp(x, L)` es el valor de la 
    > dexp(13, 20) // f(13) = 20e^(20*13)
    [1] 2.4236e-112

# Integrar
  > g = function(x) ((2*x^7)+5*x+8)
  > integrate(g, lower = 2, upper = 5)
    97668.75 with absolute error < 1.1e-09
  > integrate(g, 2, 5)
    97668.75 with absolute error < 1.1e-09
# Distribución Exponencial
-   la media es igual a la varianza y a la desviación estandar

## calcular de la media en una distibucion exponencial
a)  Utilizando R calcule la Media de una Distribución Exponencial con Q = 2
    f(x) = Qe^(-Qx) <- gráfica
    Qe^(-Qx) = 2 * exp(-2*x)
    
    > g = function(x)(2*exp(-2*x)*x)
    > media = integrate(g, lower = 0, upper = 200)
      0.5 with absolute error < 1.
    > media$value
b)  Utilizando R calcule la Media de una Distribución Exponencial con Q = 4
    > g = function(x)(2*exp(-4*x)*x)
    > media = integrate(g, lowr = 0, upper = 200)
    > media$value
      0.25
c)  Utilizando R calcula la Media de una Distribución Exponecial con Q = 5
    0.2

## ejercicios
### 1
La empresa BATERPERU fabrica baterias para automoviles, las cuales tienen un tiempo de trabajo efectivo que se distribuye segun una distribucion exponencial conun promedio tiempo de fallas igual a 480 días.
Calcule la probabilidad de que el tiempo de trabajo una de las baterías (hasta que falla) sea mayor a 600 días.

a)  Utilizando R y aplicando la formula de DEnsidad f(x)
    480 = 1/L
    L = 1/480

    > g = function(x) ((1/480)*exp(-(1/480)*x)*x)
    > area = integrate(f, 600, 10000) // el ultimo es un numero grande cualquiera
    > area
      0.2865048

b)  Utilizando R y aplicando la formula de Distribución
    > F =  exp(-(1/480)*600)
      0.2865048

c)  Utilizando pexp
    > (1/480)*exp(-(600/480)) //la probabilida que te salga exactamente 600
    > 1 - pexp(600, 1/480)
    [1] 0.2865048

### 2
La empresa BATERPERU fabrica baterias para automoviles, las cuales tienen un tiempo de trabajo efectivo que se distribuye segun una distribucion exponencial conun promedio tiempo de fallas igual a 200 días.
Calcule la probabilidad de que el tiempo de trabajo una de las baterías (hasta que falla) sea menor a 20 días o mayor a 150 días.
  200 = 1/L
  L = 1/200

  > pexp(20, 1/200) + 1 - pexp(150, 1/200)
  [1] 0.5675291

